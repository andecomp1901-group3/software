import java.util.Scanner;


public class CashRegister
{
	
	public static void main(String[] args)
	{
		String s, c, a, b, d;
		double balance, Balance = 0;
		int length = 0;
		
		System.out.println("Welcome to register!");
		Scanner in = new Scanner(System.in);
		System.out.print("Please enter cash register's float:$");
		s = in.nextLine();
		while(Double.parseDouble(s) < 0) {
			System.out.println("Please enter it in the correct format.");
			System.out.print("Please enter cash register's float:$");
			s = in.nextLine();
			continue;
		}
		balance = Double.parseDouble(s);


		System.out.print("Do you want to continue? (Yes/No)");
		a = in.nextLine();
		while(!a.equalsIgnoreCase("yes") && !a.equalsIgnoreCase("no")) {
			System.out.println("Please enter it in the correct format.");
			System.out.print("Do you want to continue? (Yes/No)");
			a = in.nextLine();
			continue;
		}


		String itemName[] = new String[99];
		String itemCost[] = new String[99];
		while(a.equalsIgnoreCase("yes")) {
			length++;


			System.out.print("Please enter the item's name:");
			s = in.nextLine();
			

			System.out.print("Please enter the item's cost:$");
			c = in.nextLine();
			while(Double.parseDouble(c) < 0) {
				System.out.println("Please enter it in the correct format.");
				System.out.print("Please enter the item's cost:$");
				c = in.nextLine();
				continue;
			}

			System.out.print("Do you need to end your purchase?(Yes/No)");
			b = in.nextLine();
			while(!b.equalsIgnoreCase("yes") && !b.equalsIgnoreCase("no")) {
				System.out.println("Please enter it in the correct format.");
				System.out.print("Do you need to end your purchase?(Yes/No)");
				b = in.nextLine();
				continue;
			}

			if(b.equalsIgnoreCase("yes")) {
				itemName[length - 1] = s;
				itemCost[length - 1] = c;
				Balance += Double.parseDouble(c);
				c = String.valueOf(Balance);
			}
			if(b.equalsIgnoreCase("no")) {
				itemName[length - 1] = s;
				itemCost[length - 1] = c;
				Balance += Double.parseDouble(c);
				continue;
			}


			Transaction trans = new Transaction(s, Double.parseDouble(c));
			double all = balance + Balance;
			System.out.print("Do you want to continue? (Yes/No)");
			a = in.nextLine();
			while(!a.equalsIgnoreCase("yes") && !a.equalsIgnoreCase("no")) {
				System.out.println("Please enter it in the correct format.");
				System.out.print("Do you want to continue? (Yes/No)");
				a = in.nextLine();
				continue;
			}

			if(a.equalsIgnoreCase("No")) {
				System.out.print("Please enter the cash amount tendered:$");
				s = in.nextLine();

				c = Double.toString(Double.parseDouble(s) - trans.getCost());
				System.out.println("Amount of change required = $" + c);
				
				
				
				// Give a gift after spending 1000 dollars or more
				if(Double.parseDouble(s) - Double.parseDouble(c) >= 1000) {  

					System.out.println("You can get a gift because you spend more than 1000 dollars");
					System.out.print("Do you need the gift we prepared for you?(Yes/No)");
					String p = in.nextLine();
					while(!p.equalsIgnoreCase("yes") && !p.equalsIgnoreCase("no")) {
						System.out.println("Please enter it in the correct format.");
						System.out.println("Do you need the gift we prepared for you?(Yes/No)");
						p = in.nextLine();
						continue;
					}
					if(p.equalsIgnoreCase("yes")) {
						System.out.println("You want a bottle of wine or a bar of chocolate?(Wine/Chocolate)"); 
						String q = in.nextLine();
						while(!q.equalsIgnoreCase("Wine") && !q.equalsIgnoreCase("Chocolate")) {
							System.out.println("Please enter it in the correct format.");
							System.out.println("You want a bottle of wine or a bar of chocolate?(Wine/Chocolate)");
							q = in.nextLine();
							continue;
						}
						if(q.equalsIgnoreCase("Wine")) {
							System.out.println("Please git your wine at window one");
						}
						if(q.equalsIgnoreCase("Chocolate")) {
							System.out.println("Please git your chocolate at window two");
						}
					}
					if(p.equalsIgnoreCase("no")) {
						System.out.println("Ok, we will not give you the gift.");
					}
				}
				
				

				System.out.println("Do you need a receipt?(Yes/No)");
				d = in.nextLine();
				while(!d.equalsIgnoreCase("yes") && !d.equalsIgnoreCase("no")) {
					System.out.println("Please enter it in the correct format.");
					System.out.print("Do you need a receipt?(Yes/No)");
					d = in.nextLine();
					continue;
				}
				double cashTendered = Double.parseDouble(s) - Double.parseDouble(c);
				if(d.equalsIgnoreCase("Yes")) {
					for(int i=0;i<length;i++) {
						System.out.println("Item's name:" + itemName[i] +" Item's cost:" + itemCost[i]);
					}
					System.out.println("The cash tendered is:$" + cashTendered);
					System.out.println("Amount of change required = $" + c);
					System.out.println("Balance of the Cash Register :$" + all);
				}
				if(d.equalsIgnoreCase("No")) {
					System.out.println("Balance of the Cash Register :$" + all);

				}
			}
		}
	}
}
